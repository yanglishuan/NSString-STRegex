//
//  NSString_STRegexTests.m
//  NSString+STRegexTests
//
//  Created by stlwtr on 15/6/15.
//  Copyright (c) 2015年 yls. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "NSString+STRegex.h"

@interface NSString_STRegexTests : XCTestCase

@end

@implementation NSString_STRegexTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPostalcode {
    NSString *postalcode = @"055350";
    BOOL rc = [postalcode isValidPostalcode];
    XCTAssertTrue(rc);
}

- (void)testChinese {
    NSString *postalcode = @"我的";
    BOOL rc = [postalcode isValidChinese];
    XCTAssertTrue(rc);
    
    postalcode = @"我";
    rc = [postalcode isValidChinese];
    XCTAssertTrue(rc);
}

- (void)testAccount {
    
    NSArray *accounts = @[
                          @"中国",
                          @"中国A",
                          @"1中",
                          @"a国",
                          @"中国的",
                          @"aa",
                          @"abcdabcdabcdabcd",
                          ];
    
    for (NSString *account in accounts) {
        BOOL rc = [account isValidWithMinLenth:2 maxLenth:16 containChinese:YES containDigtal:YES containLetter:YES containOtherCharacter:nil firstCannotBeDigtal:YES];
        XCTAssertTrue(rc);
    }
    
    accounts = @[
                 @"中",
                 @"#2",
                 @"1#",
                 @"a国#",
                 @"中国的中国的中国的中国的中国的中国的中国的中国的中国的",
                 @"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                 ];
    
    for (NSString *account in accounts) {
        BOOL rc = [account isValidWithMinLenth:2 maxLenth:16 containChinese:YES containDigtal:YES containLetter:YES containOtherCharacter:nil firstCannotBeDigtal:NO];
        NSString *msg = [NSString stringWithFormat:@"%@", account];
        char *char_msg = (char *)[msg UTF8String];
        NSLog(@"%@", msg);
        XCTAssertTrue(rc);
    }
}

@end
